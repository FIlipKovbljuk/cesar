package Ce;

public class Caesar {
	int key;

	Caesar(int key) {this.key = key;}
	Caesar(){}
	
	public char encodeChar(char symbol){
		return (char)((int)(symbol) + this.key);
	}
			
	public char decodeChar(char symbol){
		return (char)((int)(symbol) - this.key);
	}

	public String encode(String text){
		StringBuilder encoded = new StringBuilder();
	    for (int i = 0; i < text.length(); i++)
	     	encoded.append(
	     			this.encodeChar(text.charAt(i))
			);
	    return encoded.toString();
	}

	public String decode(String text){
		StringBuilder decoded = new StringBuilder();
		for (int i = 0; i < text.length(); i++)
	      	decoded.append(this.decodeChar(text.charAt(i)));
	    return decoded.toString();
	}

	public int getKey(char e, char d){
		return e - d;
	}

	
	public int getKeyFromWords(String e, String d){
		int key = -100;
		
		for(int i = 0; i < e.length(); i++){
			int key_new = this.getKey(e.charAt(i), d.charAt(i));
			if (key == -100)
				key = key_new;

			if (key_new != key)
				return -100;
		}
		this.key = key;
		return key;
	}





}
